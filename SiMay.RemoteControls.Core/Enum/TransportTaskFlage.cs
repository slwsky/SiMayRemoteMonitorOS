﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiMay.RemoteControls.Core
{
    public enum TransportTaskFlage
    {
        /// <summary>
        /// 允许
        /// </summary>
        Allow,
        /// <summary>
        /// 终止
        /// </summary>
        Abort
    }
}
